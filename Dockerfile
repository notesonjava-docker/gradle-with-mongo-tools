FROM adoptopenjdk/openjdk11:jdk-11.0.2.9-alpine


RUN apk add docker
RUN apk add --no-cache gradle
    
RUN apk add --no-cache mongodb-tools


RUN rm -rf /var/cache/apk
